title: 'Normalized average pixel cluster charge'
caption: 'For non irradiated, fully depleted detector, the pixel charge profile (normalised average pixel charge as a function of the production depth) is expected to be flat as detector is fully efficient and all charge is collected, while for irradiated detector the losses are expected due to the trapping of carriers. The losses are larger for the charges released further from the readout plane. Due to the increased irradiation of the pixel detector in Run-2, in 2018 the radiation damage effects were incorporated in the CMS pixel detector simulation. The radiation damage effects in the pixel detector are simulated by reweighting the charge of each pixel using the cluster shapes for the non irradiated and irradiated detector obtained from the PIXELAV simulation tuned to the CMS data [1,2].  The normalised average pixel charge as a function of the production depth in the silicon substrate is shown for Layer 1 of the CMS pixel barrel detector after 30.1 $fb^{-}1$ (black) and is compared to the profiles obtained using the CMS Pixel detector simulation with incorporated radiation damage effects (red). Apart from the bins close to the sensor edges, the agreement between data and MC is better the 4%.

[1] M. Swartz, "CMS Pixel Simulation", Nucl. Inst. Meth. A511 (2003) 88 doi:10.1016/S0168-9002(03)01757-1

[2] V. Chiochia et al., "Simulation of the CMS prototype silicon pixel sensors and comparison with test beam measurements CMS", IEEE Trans. Nucl. Sci (2005) 1067 Meth. A511 (2003) 88 doi:10.1109/NSSMIC.2004.1462427'
date: '2019-09-17'
tags:
- Run-2
- Collisions
- Phase-1 Pixel
- Simulation
- Radiation
- 2017